package cz.jmare.restdemo.util;

import java.util.Arrays;
import java.util.stream.Collectors;

public class TextManipulatorUtil {
    private final static String vowels = "aeiou";
    
    public static String vowelsToUpper(String str) {
        validateNotNull(str);
        char[] charArray = str.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if (vowels.contains(String.valueOf(charArray[i]))) {
                charArray[i] = Character.toUpperCase(charArray[i]);
            } else {
                charArray[i] = Character.toLowerCase(charArray[i]);
            }
        }
        return String.valueOf(charArray);
    }

    public static String normalizeSpaces(String str) {
        validateNotNull(str);
        String[] split = str.split(" +");
        return Arrays.asList(split).stream().collect(Collectors.joining(" "));
    }
    
    public static String reverseLetters(String str) {
        validateNotNull(str);
        StringBuilder sb = new StringBuilder();
        for (int i = str.length() - 1; i >= 0; i--) {
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }
    
    public static void validateNotNull(String str) {
        if (str == null) {
            throw new IllegalArgumentException("String is null");
        }
    }
}
