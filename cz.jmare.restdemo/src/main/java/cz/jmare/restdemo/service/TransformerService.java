package cz.jmare.restdemo.service;

import cz.jmare.restdemo.controller.TextTransJson;

public interface TransformerService {

    TextTransJson transform(TextTransJson textTransJson);

}
