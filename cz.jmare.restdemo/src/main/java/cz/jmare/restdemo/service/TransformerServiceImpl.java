package cz.jmare.restdemo.service;

import static cz.jmare.restdemo.util.TextManipulatorUtil.normalizeSpaces;
import static cz.jmare.restdemo.util.TextManipulatorUtil.reverseLetters;
import static cz.jmare.restdemo.util.TextManipulatorUtil.vowelsToUpper;

import org.springframework.stereotype.Service;

import cz.jmare.restdemo.controller.TextTransJson;

@Service
public class TransformerServiceImpl implements TransformerService {
    @Override
    public TextTransJson transform(TextTransJson textTransJson) {
        TextTransJson textTransJsonResp = new TextTransJson();
        String text = vowelsToUpper(textTransJson.getText());
        text = normalizeSpaces(text);
        text = reverseLetters(text);
        textTransJsonResp.setText(text);
        return textTransJsonResp;
    }
}
