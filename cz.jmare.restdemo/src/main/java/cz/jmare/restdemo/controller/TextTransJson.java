package cz.jmare.restdemo.controller;

public class TextTransJson {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
