package cz.jmare.restdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.jmare.restdemo.service.TransformerService;



@RestController
@RequestMapping("/webapi/v1/textTrans")
public class TextTransController {
    @Autowired
    private TransformerService transformerService;
    
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TextTransJson> transform(@RequestBody TextTransJson textTransJson) {
        TextTransJson textTransJsonResp = transformerService.transform(textTransJson);
        return new ResponseEntity<>(textTransJsonResp, HttpStatus.CREATED);
    }
}
