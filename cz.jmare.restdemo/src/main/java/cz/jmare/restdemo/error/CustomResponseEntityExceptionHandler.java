package cz.jmare.restdemo.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = { IllegalArgumentException.class })
    protected ResponseEntity<ErrorEntity> handleException(IllegalArgumentException ex, WebRequest request) {
        ErrorEntity errorEntity = new ErrorEntity(125);
        return new ResponseEntity<ErrorEntity>(errorEntity, HttpStatus.EXPECTATION_FAILED);
    }
}
