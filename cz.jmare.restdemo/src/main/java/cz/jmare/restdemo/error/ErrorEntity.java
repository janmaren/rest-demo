package cz.jmare.restdemo.error;

public class ErrorEntity {
    private int reasonId;

    public ErrorEntity(int reasonId) {
        super();
        this.reasonId = reasonId;
    }

    public int getReasonId() {
        return reasonId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }
}
