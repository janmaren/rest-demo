# README #

##Build

```
cd <prj_dir>
maven install
```

##Launch

```
java -jar rest-demo.jar
```

##Test

```
curl -X POST -H 'Content-Type: application/json' -i http://localhost:8080/webapi/v1/textTrans --data '{
"text" : "Nejaky   chytry text!"
}'
```
